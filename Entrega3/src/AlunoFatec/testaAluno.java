package AlunoFatec;

import javax.swing.JOptionPane;

public class testaAluno {
	public static void main(String[] args) {
		Aluno a1 = new Aluno("Pedro");
		a1.setCurso("Banco de Dados");
		a1.setQualidade("Inteligente");
		a1.setDefeito("Preguiçoso");
		a1.setSemestre(2);
		a1.setNota(6.0);
		
		a1.fimDeSemeste();
		a1.irParaAula();
		a1.trancarCurso();
		
		Aluno[] newAluno = new Aluno[5];
		for(int i=0; i < 5; i++) {
			String nome = JOptionPane.showInputDialog("Digite o nome do aluno ");
			newAluno[i] = new Aluno(nome);
			if(i%2 == 0) {
				newAluno[i].setQualidade("Inteligente");
				newAluno[i].setDefeito("Preguiçoso");
				newAluno[i].setNota(7.0);
				newAluno[i].setSemestre(3);
			}else {
				newAluno[i].setQualidade("Bonito");
				newAluno[i].setDefeito("Arrogante");
				newAluno[i].setNota(4.5);
				newAluno[i].setSemestre(5);
			}
			newAluno[i].fimDeSemeste();
		}
	}
}
