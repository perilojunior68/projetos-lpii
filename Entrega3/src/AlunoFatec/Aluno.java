package AlunoFatec;

public class Aluno {
	private String nome;
	private String curso;
	private String qualidade;
	private String defeito;
	private int semestre;
	private Double nota;
	public Aluno(String nome) {
		this.nome = nome;
	}
	public void setCurso(String c) {
		curso = c;
	}
	public void setQualidade(String q) {
		qualidade = q;
	}
	public void setDefeito(String d) {
		defeito = d;
	}
	public void setSemestre(int s) {
		semestre = s;
	}
	public void setNota(Double n) {
		nota = n;
	}
	public String getCurso() {
		return curso;
	}
	public String getQualidade() {
		return qualidade;
	}
	public String getDefeito() {
		return defeito;
	}
	public int getSemestre() {
		return semestre;
	}
	public Double getNota() {
		return nota;
	}
	public void fimDeSemeste() {
		if(getNota() < 6) {
			System.out.println("O aluno " + nome + " continua no semestre " + getSemestre());
		}else {
			int novoSemestre;
			novoSemestre = getSemestre() + 1;
			System.out.println("O aluno " + nome + " foi para o semestre " + novoSemestre);
		}
	}
	public void irParaAula() {
		System.out.println("O aluno foi para aula do curso " + getCurso());
	}
	public void trancarCurso() {
		System.out.println("O aluno desistiu do curso :(");
	}

}
