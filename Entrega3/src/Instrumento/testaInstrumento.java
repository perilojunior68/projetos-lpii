package Instrumento;

public class testaInstrumento {
	public static void main(String[] args) {
		Instrumento i1 = new Instrumento("Teclado", "Teclas", "baixo");
		i1.setMaterial("pl�stico");
		
		i1.tocar();
		i1.afinar();
		i1.guardar();
		
		Instrumento[] newI = new Instrumento[3];
		newI[0] = new Instrumento("Violao", "Cordas", "Acustico");
		newI[0].tocar();
		
		newI[1] = new Instrumento("Violino", "Cordas", "Acustico");
		newI[1].afinar();
		
		newI[2] = new Instrumento("Bateria", "Tambor", "Alto");
		newI[2].guardar();
		
	}
}
