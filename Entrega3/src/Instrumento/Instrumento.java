package Instrumento;

public class Instrumento {
	private String nome;
	private String material;
	private String tipo;
	private String altura;
	
	public Instrumento(String nome, String tipo, String altura) {
		this.nome = nome;
		this.tipo = tipo;
		this.altura = altura;
	}
	public void setMaterial(String m) {
		material = m;
	}
	public String getMaterial() {
		return material;
	}
	public void afinar() {
		System.out.println("O instrumento foi afinado");
	}
	public void tocar() {
		System.out.println("Uma m�sica foi tocada");
	}
	public void guardar() {
		System.out.println("O instrumento foi guardado");
	}
}
