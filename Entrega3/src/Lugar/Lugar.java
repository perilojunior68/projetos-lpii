package Lugar;

public class Lugar {
	private String pais;
	private String cidade;
	private String estado;
	private String clima;
	private String fuso;
	
	public Lugar(String cPais, String cClima, String fuso){
		this.fuso = fuso;
		clima = cClima;
		pais = cPais;
	}
	public void setCidade(String c) {
		cidade = c;
	}
	public void setEstado(String e) {
		estado = e;
	}
	public String getCidade() {
		return cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void chover() {
		System.out.println("Esta chovendo no: " + getCidade());
	}
	public void evento() {
		System.out.println("O presidente decidiu fazer evento no estado de: " + getEstado() + " na cidade de:" + getCidade());
	}
	public void desastre() {
		System.out.println("Ocorreu um desastre na cidade!!!");
	}

}
