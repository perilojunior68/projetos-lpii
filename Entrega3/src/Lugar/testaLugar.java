package Lugar;

import javax.swing.JOptionPane;

import AlunoFatec.Aluno;

public class testaLugar {
	public static void main(String[] args) {
		Lugar novoLugar = new Lugar("3.0MT","Tropical","Brasil");
		novoLugar.setEstado("S�o Paulo");
		novoLugar.setCidade("S�o Jos� dos Campos");
		
		System.out.println("Minha cidade �: "+ novoLugar.getCidade());
		novoLugar.chover();
		novoLugar.evento();
		novoLugar.desastre();
		
		Lugar[] l1 = new Lugar[3];
		for(int i = 0; i < 3; i++) {
			String fuso = JOptionPane.showInputDialog("Digite o fuso do local ");
			String clima = JOptionPane.showInputDialog("Digite o clima do local ");
			String pais = JOptionPane.showInputDialog("Digite o pa�s do local ");
			
			l1[i] = new Lugar(fuso, clima, pais);
			String estado = JOptionPane.showInputDialog("Digite o Estado do local ");
			l1[i].setEstado(estado);
			String cidade = JOptionPane.showInputDialog("Digite a cidade do local ");
			l1[i].setCidade(cidade);
			
			if(i == 0) {
				l1[i].chover();
			}else if(i == 1) {
				l1[i].evento();
			}else {
				l1[i].desastre();
			}
			
		}
	}
}
