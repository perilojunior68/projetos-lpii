package Calcado;

public class Calcado {
	private String marca;
	private String modelo;
	private int tamanho;
	private String cor;
	private String material;
	private double preco;
	
	public Calcado(String marca, String modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}
	public void setTamanho(int t) {
		tamanho = t;
	}
	public void setCor(String c) {
		cor = c;
	}
	public void setMaterial(String m) {
		material = m;
	}
	public void setPreco(double p) {
		preco = p;
	}
	public int getTamanho() {
		return tamanho;
	}
	public String getCor() {
		return cor;
	}
	public String getmaterial() {
		return material;
	}
	public double getPreco() {
		return preco;
	}
	
	public void calcar() {
		System.out.println("O cal�ado foi cal�ado!");
	}
	public void trocarPreco(double novoPreco) {
		double diferenca = novoPreco - getPreco();
		System.out.println("O novo pre�o � " + getPreco() + " sendo " + diferenca + " reais a diferen�a entre o valor anterior");
	}
	public void customizar(String novaCor) {
		System.out.println("O sapato foi custumizado da cor " + getCor() + " para a cor " + novaCor);
	}
	
}
