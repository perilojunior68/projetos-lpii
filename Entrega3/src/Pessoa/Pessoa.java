package Pessoa;

public class Pessoa {
	private String nome;
	private String sexo;
	private int idade;
	private String profissao;
	
	public Pessoa(String nome, String sexo) {
		this.nome = nome;
		this.sexo = sexo;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public void setIdade(int i) {
		idade = i;
	}
	public int getIdade() {
		return idade;
	}
	public String getProfissao() {
		return profissao;
	}
	public void dancar() {
		System.out.println("O " + nome + " esta dan�ando loucamente!");
	}
	public void aniversario() {
		int novaIdade = getIdade() + 1;
		System.out.println("O" + nome + " fez "+ novaIdade + " anos");
	}
	public void trabalhar() {
		System.out.println("O " + nome + " trabalhou agora como " + getProfissao());
	}
	
}
