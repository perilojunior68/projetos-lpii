package Pessoa;

import javax.swing.JOptionPane;

public class testePessoa {
	public static void main(String[] args) {
		Pessoa novaPessoa = new Pessoa("Alan", "M");
		novaPessoa.setIdade(25);
		novaPessoa.setProfissao("Youtuber");
		novaPessoa.trabalhar();
		novaPessoa.aniversario();
		novaPessoa.dancar();
		
		Pessoa[] newPessoa = new Pessoa[3];
		for(int i = 0; i < 3; i++) {
			String nome = JOptionPane.showInputDialog("Digite o nome da pessoa ");
			String sexo = JOptionPane.showInputDialog("Digite o sexo da pessoa ");
			newPessoa[i] = new Pessoa(nome, sexo);
			String profissao = JOptionPane.showInputDialog("Digite a profissao da pessoa ");
			newPessoa[i].setProfissao(profissao);
			String idade = JOptionPane.showInputDialog("Digite a idade da pessoa ");
			newPessoa[i].setIdade(Integer.parseInt(idade));
			
			newPessoa[i].aniversario();
			newPessoa[i].trabalhar();
			newPessoa[i].dancar();
		}
	}
}
