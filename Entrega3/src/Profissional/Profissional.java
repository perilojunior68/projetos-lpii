package Profissional;

public class Profissional {
	private String nome;
	private String cargo;
	private String profissao;
	private int atuacao;
	public Profissional(String n) {
		nome = n;
	}
	public void setCargo(String c) {
		cargo = c;
	}
	public void setProfissao(String p) {
		profissao = p;
	}
	public void setAtuacao(int a) {
		atuacao = a;
	}
	public String getCargo() {
		return cargo;
	}
	public String getProfissao() {
		return profissao;
	}
	public int getAtuacao() {
		return atuacao;
	}
	
	public void quemE() {
		System.out.println("Ola meu nome � " + nome + " e trabalho como " + getProfissao() + " sendo um " + getCargo() + " com " + getAtuacao() + " anos de experi�ncia");
	}
	public void verificaCurriculo() {
		int info = getAtuacao();
		if(info < 1) {
			System.out.println("Ele rec�m iniciou a carreira");
		}else if(info > 1 && info < 3) {
			System.out.println("Ele j� possui certa  experi�ncia na carreia");
		}else {
			System.out.println("Essa cara � do bom viu");
		}
	}
	public void promocao() {
		System.out.println("Parab�ns ! Voc� foi promovido a " + getCargo());
	}
}
