package Profissional;

import javax.swing.JOptionPane;

public class testaProfissional {
	public static void main(String[] args) {
		Profissional p1 = new Profissional("Ana");
		p1.setAtuacao(5);
		p1.setCargo("Chefe");
		p1.setProfissao("Engenheira civil");
		
		p1.quemE();
		p1.promocao();
		p1.verificaCurriculo();
		
		Profissional[] newPro = new Profissional[3];
		
		for(int i = 0; i < 3; i++) {
			String nome = JOptionPane.showInputDialog("Digite o nome ");
			newPro[i] = new Profissional(nome);
			String atuacao = JOptionPane.showInputDialog("Digite o tempo de atua��o ");
			newPro[i].setAtuacao(Integer.parseInt(atuacao));
			String cargo = JOptionPane.showInputDialog("Digite o cargo ocupado ");
			newPro[i].setCargo(cargo);
			String profissao = JOptionPane.showInputDialog("Digite a profiss�o ");
			newPro[i].setProfissao(profissao);
			
			newPro[i].quemE();
			newPro[i].promocao();
			newPro[i].verificaCurriculo();
			
		}
	}
}
