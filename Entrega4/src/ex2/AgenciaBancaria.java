package ex2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AgenciaBancaria {
	public static ArrayList<Conta> contaList = new ArrayList<Conta>();
	public static ArrayList<Pessoa> pessoaList = new ArrayList<Pessoa>();
	
	public static void adicionarConta() {
		Set<String> cpfClientes;
		cpfClientes = new HashSet<String>();
		String cpf = "123456789";
		if (!cpfClientes.contains(cpf)) {
			Pessoa p = new Pessoa();
			p.setCpf(cpf);
			p.setNome("Fulano");
			p.setConta(258741);
			
			Conta c = new Conta();
			c.setCpf(p.getCpf());
			c.setNumero(p.getConta());
			c.setDinheiro(100.00);
			
			pessoaList.add(p);
			contaList.add(c);
			cpfClientes.add(cpf);
		}else{
			System.out.println("Conta j� cadastrada");
		}
	}
	public static void removerConta() {
		contaList.remove(0);
		System.out.println("Conta deletada");
	}
	public static void listarDadosPessoa(String cpf){
		for(Pessoa pessoa : pessoaList) {
			if(pessoa.getCpf().equals(cpf)) {
				System.out.println("Nome: " + pessoa.getNome() + " e a conta: " + pessoa.getConta());
			}
		}
	}
	
	public static void main(String[] args) {
		AgenciaBancaria.adicionarConta();
		Conta contaObj;
		contaObj = contaList.get(0);
		AgenciaBancaria.listarDadosPessoa(contaObj.getCpf());
		
		AgenciaBancaria.removerConta();
	
	}
}
