package ex1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;


public class Concessionaria {
	public static ArrayList<Carro> car = new ArrayList<Carro>();
	public static HashMap<String, Double> tabela = new HashMap<String, Double>();
	private static Carro carro;
	
	public static void cadCarro() {
		
		String modelo;
		String marca;
		
		modelo = JOptionPane.showInputDialog("Informe o modelo do carro");
		marca = JOptionPane.showInputDialog("Informe a marca do carro");
		carro = new Carro(marca, modelo);
		
		carro.setCor(JOptionPane.showInputDialog("Qual a cor do carro?"));
		carro.setPlaca(JOptionPane.showInputDialog("Qual a placa do carro?"));
		carro.setAno(Integer.parseInt(JOptionPane.showInputDialog("Qual o ano do carro?")));
		carro.setPreco(Double.parseDouble(JOptionPane.showInputDialog("Informe o pre�o do carro")));
		
		car.add(carro);
		
		tabela.put(carro.getPlaca(), carro.getPreco());
		
	}
	
	public static void listarCarros() {
		for(int i = 0; i < car.size(); i++) {
			System.out.println(car.get(i));
		}
	}
	
	public static void imprimirTabelapreco() {
		Iterator it = tabela.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			System.out.println("A placa do carro � " + pair.getKey() + " e o pre�o " + pair.getValue());
			it.remove();
		}
	}
	
	public static void listarDados(String placa) {
		//verif = car.stream().filter(c -> c.getPlaca().equals(placa)).findFirst().isPresent();
		for(Carro carro : car) {
			if(carro.getPlaca().equals(placa)) {
	            System.out.println("O carro escolhido possui a placa: " + carro.getPlaca() + " e modelo �  " + carro.getModelo() + " e custa " + carro.getPreco());
	        }
		}
	}
	
	public static void main(String[] args) {
		
		int qtde;
		qtde = Integer.parseInt(JOptionPane.showInputDialog("Quantos carros ser�o cadastrados?"));
		for(int i = 0; i < qtde; i++) {
			Concessionaria.cadCarro();
		}
		Concessionaria.listarCarros();
		
		Object[] item = new Object [qtde];
		Object selecionado=new Object();
		
		for (int i=0;i<qtde;i++)	item[i]=carro.getPlaca();
		selecionado = JOptionPane.showInputDialog(null, "Escolha placa", "Opção", JOptionPane.INFORMATION_MESSAGE, null, item, item[0]); 
		listarDados(selecionado.toString());
		Concessionaria.imprimirTabelapreco();
	}
}
