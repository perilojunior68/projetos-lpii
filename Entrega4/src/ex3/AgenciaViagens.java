package ex3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;


public class AgenciaViagens {
	public static ArrayList<Lugar> lugar = new ArrayList<Lugar>();
	public static ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
	
	public static void adicionarViagem() {
		Lugar[] l = new Lugar [3];
		
		for (int i = 0; i < 2; i++) {
			l[i] = new Lugar();
			l[i].setCidade(JOptionPane.showInputDialog("Qual a cidade?"));
			l[i].setEstado(JOptionPane.showInputDialog("Qual o estado?"));
			l[i].setPais(JOptionPane.showInputDialog("Qual o pa�s?"));
			
			lugar.add(l[i]);
		}
	}
	public static void listarDadosDestinos() {
		for(Lugar lugar : lugar) {
			System.out.println("A cidade " + lugar.getCidade() + " o estado " + lugar.getEstado() + " e o pa�s " + lugar.getPais());
		}
		
	}
	public static void listarDadosPessoa(String cpf) {
		for(Pessoa pessoa : pessoas) {
			if(pessoa.getCpf().equals(cpf)){
				System.out.println("O nome da pessoa � " + pessoa.getNome());
			}
		}
	}
	public static void adicionarCliente() {
		Set<String> cpf;
		cpf = new HashSet<String>();
		String cpfCad = "123456789";
		if (!cpf.contains(cpfCad)) {
			
		Pessoa p = new Pessoa();
		p.setCpf(cpfCad);
		p.setNome("Adalberto");
		pessoas.add(p);
		cpf.add(cpfCad);
		}
	}
	
	public static void main(String[] args) {
		AgenciaViagens.adicionarCliente();
		AgenciaViagens.adicionarViagem();
		AgenciaViagens.listarDadosDestinos();
		
		for (Pessoa pessoa : pessoas) {
			AgenciaViagens.listarDadosPessoa(pessoa.getCpf());
		}
	}
}
